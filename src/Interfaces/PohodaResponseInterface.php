<?php

namespace PohodaServerApi\Interfaces;

/**
 * Interface for documentation purposes. It defines how response data looks like.
 * @package    
 * @author     Martin Kovacik <martin.kovacik@esoul.systems>
 * 
 */
abstract class PohodaResponseInterface {

    /**
     * Success state
     * @var bool 
     */
    public $success;
    
    /**
     * HTTP code
     * @var int 
     */
    public $code;
    
    /**
     * error array. 
     * @var array | StdClass | null 
     */
    public $errors;
    
    /**
     * data recieved
     * @var array | StdClass | null 
     */
    public $data;
    
    /**
     * message
     * @var string 
     */
    public $message;
    
    
}
