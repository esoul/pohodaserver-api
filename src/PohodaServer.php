<?php
namespace PohodaServerApi;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Utils;

/**
 * Description of PohodaServer
 * @package    
 * @author     Martin Kovacik <martin.kovacik@esoul.systems>
 * 
 */
class PohodaServer {
    //put your code here
    
    /**
     *
     * @var string 
     */
    protected $uri;
    
    /**
     *
     * @var array 
     */
    protected $token;
    
    /**
     *
     * @var GuzzleHttp\Client
     */
    protected $client;
    
    protected $errors = [];
    
    
    function __construct( string $uri ) {
        $this->uri = rtrim( trim( $uri ), "/" ) . "/";
        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * retrieve access token from pohodaserver
     * @param string $secret
     * @param int $id
     * @return array || null
     */
    public function makeToken( string $secret, int $id ){
        
        try{
            $response = $this->client->post( "{$this->uri}oauth/token", [
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $id,
                    'client_secret' => $secret,
                    'scope' => '*',
                ],
            ]);
        }
        catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return null;
        }
        
        $this->token = ( json_decode( ( string ) $response->getBody(), true ) );
    }
    
    
    //////////////////////////////////// get methods
    
    public function getUpdateOrderFields( $accessToken = null ){
        return $this->getFields( "{$this->uri}api/v1/pohoda/order/attributes" , $accessToken);
    }
    
    /**
     * Get all payments from Stormware Pohoda DB
     * @param array $accessToken
     * @return json?
     */
    public function getPaymentAttributes( $accessToken = null ){
        return $this->getFields( "{$this->uri}api/v1/pohoda/payments/attributes" , $accessToken);
    }

    /**
     * Get all payments from Stormware Pohoda DB
     * @param array $accessToken
     * @return json?
     */
    public function getDeliveryAttributes( $accessToken = null ){
        return $this->getFields( "{$this->uri}api/v1/pohoda/delivery/attributes" , $accessToken);
    }

    /**
     * Get all payments from Stormware Pohoda DB
     * @param array $accessToken
     * @return json?
     */
    public function getStatesAttributes( $accessToken = null ){
        return $this->getFields( "{$this->uri}api/v1/pohoda/states/attributes" , $accessToken);
    }
    
    /**
     * Get all products from Stormware Pohoda DB
     * @param int $page
     * @param array $accessToken
     * @return json?
     */
    public function getSimpleProducts( $page = null, $accessToken = null ){
        return $this->getFields( "{$this->uri}api/v1/pohoda/products/simple?page=$page" , $accessToken);
    }

    /**
     * Get invoice PDF from pohodaserver by ordernumber
     * @param string $ordernumber
     * @param string $filename
     * @param $accessToken
     * @return false|int
     */
    public function getInvoice( string $ordernumber, string $filename, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;

        return $this->action( "get", "{$this->uri}api/v1/pohoda/invoices/pdf", $this->prepareUpdateBody( $accessToken, [ "ordernumber" => $ordernumber ] ) );
    }

    /**
     * Get invoice PDF from pohodaserver by ordernumber
     * @param string $ordernumber
     * @param string $filename
     * @param $accessToken
     * @return false|int
     */
    public function getCreditNote( string $ordernumber, string $filename, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;

        return $this->action( "get", "{$this->uri}api/v1/pohoda/creditnotes/pdf", $this->prepareUpdateBody( $accessToken, [ "ordernumber" => $ordernumber ] ) );
    }

    /**
     * Get Invoice numbers of latest invoices
     * @param int $minutes
     * @param $accessToken
     * @return \Psr\Http\Message\ResponseInterface|null
     */
    public function getLatestInvoices( int $minutes = 1440, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;
        return $this->action( "get", "{$this->uri}api/v1/pohoda/invoices/latestNumbers/$minutes", $this->prepareHeaders( $accessToken, [ "application/json" ] ) );
    }

    /**
     * Get numbers of latest creditNotes - Dobropis
     * @param int $minutes
     * @param $accessToken
     * @return \Psr\Http\Message\ResponseInterface|null
     */
    public function getLatestCreditNotes( int $minutes = 1440, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;
        return $this->action( "get", "{$this->uri}api/v1/pohoda/creditnotes/latestNumbers/$minutes", $this->prepareHeaders( $accessToken, [ "application/json" ] ) );
    }
    
    /**
     * Send GET request and return config
     * @param string $url
     * @param array | null $accessToken
     * @return StdClass | null
     */
    private function getFields( $url, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;
        
        try{
            $response = $this->client->get( $url, $this->prepareHeaders( $accessToken, [ "application/json" ] ) );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return null;
        }
        return ( json_decode( ( string ) $response->getBody() )->data );
    }

    /**
     * Get product attributes from Stormware Pohoda DB
     * @param array $accessToken
     * @return json?
     */
    public function getUpdateProductFields( $accessToken = null ){
        return $this->getFields( "{$this->uri}api/v1/pohoda/product/attributes" , $accessToken);
    }

    /**
     * Get preorder flags from all Pohoda products in main stock
     * @param int $page
     * @param array $accessToken
     * @return json?
     */
    public function getPreorderFlags( $accessToken = null ){
        return json_decode($this->getFields( "{$this->uri}api/v1/pohoda/product/preorder" , $accessToken));
    }
    
    
    //////////////////////// checking methods
    
    /**
     * Return info if order can be recreated.
     * 
     * @param string $ordernumber
     * @param string $accessToken
     * @return StdClass | null
     */
    public function canRecreateOrder( string $ordernumber, $accessToken = null ){
        return $this->getFields( "{$this->uri}api/v1/pohoda/order/$ordernumber/canRecreate" , $accessToken);
    }
    
    ///////////////////////////////////// update methods
    
    /**
     * Updates order data via PohodaServer API. Returns decoded message from server.
     * @param string $orderID
     * @param array $attributes
     * @param array $accessToken
     * @return Interfaces\PohodaResponseInterface | null
     */
    public function updateOrder( string $orderID, array $attributes, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;
        
        try{
            $response = $this->client->post(
                    "{$this->uri}api/v1/pohoda/order/{$orderID}",
                    $this->prepareUpdateBody( $accessToken, $attributes )
            );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return json_decode( ( string ) $e->getResponse()->getBody() );
        }

        return json_decode( ( string ) $response->getBody() );
    }
    
    /**
     * @todo - doc + logic
     */
    public function updateOrderPayment( string $orderID, array $data, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;
        
        try{
            $response = $this->client->post(
                    "{$this->uri}api/v1/pohoda/order/{$orderID}/payment",
                    $this->prepareUpdateBody( $accessToken, $data )
            );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return json_decode( ( string ) $e->getResponse()->getBody() );
        }

        return json_decode( ( string ) $response->getBody() );
    }
    
    /**
     * Sends XML with payment data to PohodaServer custom endpoint where data are set into DB directly.
     * @param string $xml
     * @param string $filename
     * @param string $orderNumber
     * @param array  $accessToken
     * @return ??? @todo
     */
    public function updateOrderPaid( $xml, string $filename, string $orderNumber, $accessToken = null ){
        
        $accessToken = $accessToken ?? $this->token;
        $file = Utils::streamFor( $xml );
        $request = $this->prepareHeaders( $accessToken ) + $this->prepareExportBody( $file, $filename, 0 );
        
        try{
            $response = $this->client->post( "{$this->uri}api/v1/pohoda/order/{$orderNumber}/paymentDataImport", $request );

        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return json_decode( ( string ) $e->getResponse()->getBody() );
        }
        
        return ( json_decode( ( string ) $response->getBody() ) );
    }

    /**
     * @todo - doc + logic
     */
    public function appendOrderColumns( string $orderID, array $data, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;

        try{
            $response = $this->client->post(
                "{$this->uri}api/v1/pohoda/order/{$orderID}/append",
                $this->prepareUpdateBody( $accessToken, $data )
            );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return json_decode( ( string ) $e->getResponse()->getBody() );
        }

        return json_decode( ( string ) $response->getBody() );
    }

    /**
     * Updates product data via PohodaServer API. Returns decoded message from server.
     * @param string $productID
     * @param array $attributes
     * @param array $accessToken
     * @return Interfaces\PohodaResponseInterface | null
     */
    public function updateStock( string $productID, array $attributes, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;

        try{
            $response = $this->client->post(
                "{$this->uri}api/v1/pohoda/product/{$productID}",
                $this->prepareUpdateBody( $accessToken, $attributes )
            );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return json_decode( ( string ) $e->getResponse()->getBody() );
        }

        return json_decode( ( string ) $response->getBody() );
    }

    /**
     * Upload images via PohodaServer
     * @param $attributes
     * @param null $accessToken
     * @return Interfaces\PohodaResponseInterface | null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function uploadImages( $attributes, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;

        try{
            $response = $this->client->post( "{$this->uri}api/v1/pohoda/images/",
                $this->prepareUpdateBody( $accessToken, $attributes ));
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return json_decode( ( string ) $e->getResponse()->getBody() );
        }

        return json_decode( ( string ) $response->getBody() );
    }
    

    ///////////////////////// delete methods 
    
    /**
     * Storno order via PohodaServer
     * @param int $id
     * @param array $accessToken
     * @return Interfaces\PohodaResponseInterface | null
     */
    public function stornoOrder( $id, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;
        $request = $this->prepareHeaders( $accessToken,  [ "application/json" ] ) + [ "form_params" => [] ];

        try{
            $response = $this->client->post( "{$this->uri}api/v1/pohoda/order/$id/storno", $request );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return json_decode( ( string ) $e->getResponse()->getBody() );
        }
        
        return json_decode( ( string ) $response->getBody() );
    }
    
    /////////////////////////////////// bhit methods
    
    /**
     * Upload file into filesystem via PohodaServer.
     * file will be uploaded into special folder. BHIT extension can read XMLs from that folder and do his magic.
     * @param type $file
     * @param string $name
     * @param array $accessToken
     * @return string
     */
    public function uploadBhitXML( $file, string $name, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;
        $file = Utils::streamFor( $file );
        
        $request = $this->prepareHeaders( $accessToken,  [ "application/json" ] ) + $this->prepareExportBody( $file, $name );
        //dd( $request );
        try{
            $response = $this->client->post( "{$this->uri}api/v1/pohoda/bhit", $request );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return null;
        }
        
        return ( ( ( string ) $response->getBody() ) );
    }
    
    /**
     * Import orders with it's delivery types from Pohoda Server
     */
    public function importDeliveries(){
         $accessToken = $accessToken ?? $this->token;
         $request = $this->prepareHeaders( $accessToken,  [ "application/json" ] );
         
         try{
            $response = $this->client->post( "{$this->uri}api/v1/pohoda/delivery/parcels", $request );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return json_decode( ( string ) $e->getResponse()->getBody() );
        }
        
        return ( json_decode( ( string ) $response->getBody() ) );
    }
    
    /**
     * Import carrier names and how many times they were used 
     */
    public function importCarrierCounts( $startOrderId = null, $endOrderId = null ){
        $accessToken = $accessToken ?? $this->token;
         $request = $this->prepareHeaders( $accessToken,  [ "application/json" ] );
         
         try{
            $response = $this->client->post( "{$this->uri}api/v1/pohoda/delivery/carrierscount/$startOrderId/$endOrderId", $request );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return json_decode( ( string ) $e->getResponse()->getBody() );
        }
        
        return ( json_decode( ( string ) $response->getBody() ) );
    }
    
    /**
     * Import orders & products that are partially completed
     * @param Carbon $dateFrom
     * @param Carbon $dateTo
     * @return \stdClass
     */
    public function importPartialOrders( $dateFrom, $dateTo, $currencyCode = null ){
        //pohoda/order/partialOrders
        
        $accessToken = $accessToken ?? $this->token;
        $request = $this->prepareUpdateBody( $accessToken, ["dateFrom" => $dateFrom, "dateTo" => $dateTo, "currencyCode" => $currencyCode ] );
        
        try{
            $response = $this->client->post( "{$this->uri}api/v1/pohoda/order/partialOrders", $request );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return json_decode( ( string ) $e->getResponse()->getBody() );
        }
        
        return ( json_decode( ( string ) $response->getBody() ) );
    }
    
    /**
     * Import orders XML. 
     * XML structure is similar as stavyOBJ.xml
     * @deprecated
     */
    public function importOrderStates( $accessToken = null ){
        return $this->importOrderData( 5760, $accessToken );
    }

    /**
     * Import orders XML.
     * XML structure is similar as stavyOBJ.xml
     */
    public function importOrderData( int $minutes = 5760, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;
        return $this->getFields( "{$this->uri}api/v1/pohoda/order/orders/$minutes" , $accessToken);
    }

    /**
     * @return void
     */
    public function importOrdersXML( $file, $accessToken = null ){
        $time = Carbon::now()->timestamp;
        $response = $this->export( $file, "receivedOrder_$time.xml", "orderRecieved", 1, $accessToken );
        return $response;
    }

    /**
     * Send XMl with request and return XML response from pohoda.
     * @param $file
     * @param array $accessToken
     * @return string|null
     */
    public function importUserData( $file, $accessToken = null ){
        $time = Carbon::now()->timestamp;
        $response = $this->export( $file, "adressBook_$time.xml", "addresBook", 1, $accessToken );
        return $response;
    }
    
    ///////////////////////////////////// export methods
    
    /**
     * Export invoice into pohoda server
     * @param \SimpleXMLElement $invoice
     * @param string $name
     * @param array $accessToken
     * @return string || null
     */
    public function exportInvoice( $invoice, string $name, int $checkDuplicates = 1, $accessToken = null ){
        return $this->export( $invoice, $name, "invoice", $checkDuplicates, $accessToken );
    }
    
    /**
     * Export invoice into pohoda server
     * @param \SimpleXMLElement $order
     * @param string $name
     * @param array $accessToken
     * @return \SimpleXMLElement || null
     */
    public function exportOrder( $order, string $name, int $checkDuplicates = 1, $accessToken = null ){
        $response = $this->export( $order, $name, "order", $checkDuplicates, $accessToken );
        return empty( $response ) ? $response : simplexml_load_string( $response );
    }

    /**
     * Export stock into pohoda server
     * @param \SimpleXMLElement $stock
     * @param string $name
     * @param array $accessToken
     * @return string || null
     */
    public function doStockAction( $stock, string $name, int $checkDuplicates = 1, array $accessToken = null ){
        return $this->export( $stock, $name, "stock", $checkDuplicates, $accessToken );
    }
    
    
    
    /////////////////////////////////////////
    
    /**
     * Export file into Pohoda Server and get response
     * @param type $file
     * @param string $name
     * @param string $type
     * @param array $accessToken
     * @return string
     */
    private function export( $file, string $name, string $type, int $checkDuplicates = 1, $accessToken = null ){
        $accessToken = $accessToken ?? $this->token;
        $file = Utils::streamFor( $file );
        $request = $this->prepareHeaders( $accessToken ) + $this->prepareExportBody( $file, $name, $checkDuplicates );
        
        try{
            $response = $this->client->post( "{$this->uri}api/v1/pohoda/pair/{$type}", $request );
            
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            
            $this->errors[] = $e;
            return null;
        }
        
        return ( ( ( string ) $response->getBody() ) );
    }
    
    
    /**
     * prepare guzzlehttp array for request
     * @param array $accessToken
     * @param string $file
     * @param string $filename
     * @param string $name
     * @return array
     */
    private function prepareExportBody( $file, $filename, int $checkDuplicates = 1 ){
        return [
            'multipart' => [[
                    'Content-type' => 'multipart/form-data',
                    'name' => "file",
                    'contents' => $file,
                    'filename' => $filename,
                ],
                [
                    'Content-type' => 'multipart/form-data',
                    'name' => "checkDuplicates",
                    'contents' => $checkDuplicates,
                ],
            ],
        ];
    }
    
    /**
     * Prepare guzzlehttp request which is compatible with PohodaServer endpoint: pohoda/order
     * @param type $accessToken
     * @param array $attributes
     * @return array
     */
    private function prepareUpdateBody( $accessToken, array $attributes ){
        
        return $this->prepareHeaders( $accessToken, [ "application/json" ] ) + [
            'json' => $attributes
        ];
    }
    
    private function prepareHeaders( array $accessToken, $accepts = ["application/json", "application/xml" ] ){
        return [
            'headers' => [
                'Accept' => implode(", ", $accepts ),
                'X-Authorization' => "{$accessToken['token_type']} ". $accessToken["access_token"],
            ],
        ];
    }

    private function action( $method, $url, $data ){
        try{
            $response = $this->client->$method( $url, $data );
        } catch( \GuzzleHttp\Exception\RequestException $e ){
            $this->errors[] = $e;
            return $e->getResponse();
        }

        return $response;
    }
    
    ////////////
    
    function getToken() {
        return $this->token;
    }
        
    /**
     * Set access token if already have one
     * @param string $token
     */
    public function setToken( array $token ){
        $this->token = $token;
    }
    
    function getErrors() {
        return $this->errors;
    }


}
